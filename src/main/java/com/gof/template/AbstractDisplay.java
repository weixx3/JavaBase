package com.gof.template;

public abstract class AbstractDisplay {

	public final void dispaly() {
		open();
		for (int i = 0; i < 5; i++) {
			print();
		}
		close();
	}

	public abstract void open();

	public abstract void print();

	public abstract void close();
}
