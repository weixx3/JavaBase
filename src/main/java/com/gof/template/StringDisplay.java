package com.gof.template;

public class StringDisplay extends AbstractDisplay {
	private String s;
	private int i;

	public StringDisplay(String s) {
		this.s = s;
		this.i = s.length();
	}

	@Override
	public void open() {
		println();
	}

	private void println() {
		System.out.print("+");
		for (int i = 0; i < this.i; i++) {
			System.out.print("-");
		}
		System.out.println("+");
	}

	@Override
	public void print() {
		System.out.println("|" + this.s + "|");
	}

	@Override
	public void close() {
		println();
	}

}
