package com.gof.template;

public class CharDispaly extends AbstractDisplay {
	private char c;

	public CharDispaly(char c) {
		this.c = c;
	}

	@Override
	public void open() {
		System.out.print(">>");
	}

	@Override
	public void print() {
		System.out.print(c);
	}

	@Override
	public void close() {
		System.out.println("<<");
	}

}
