package com.gof.factoryMethod;

public class IDCardProduct extends Product {
	private String owner;

	public IDCardProduct(String owner) {
		System.out.println("-- 生成 -- " + owner + " -- --");
		this.owner = owner;
	}

	@Override
	public void use() {
		System.out.println("-- 使用 -- " + owner + " -- --");
	}

}
