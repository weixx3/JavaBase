package com.gof.factoryMethod;

public class Main {

	public static void main(String[] args) {
		Factory factory = new IDCardFactory();
		Product p1 = factory.create("小米");
		Product p2 = factory.create("锤子");
		Product p3 = factory.create("华为");
		p1.use();
		p2.use();
		p3.use();
	}
}
