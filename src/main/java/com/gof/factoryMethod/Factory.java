package com.gof.factoryMethod;

public abstract class Factory {

	public final Product create(String s) {

		Product product = createProduct(s);
		return product;
	}

	public abstract Product createProduct(String s);

}
