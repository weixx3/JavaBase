package com.gof.singleton;

public class Singleton {
	private final static Singleton singleton = new Singleton();

	public static Singleton getSingleton() {
		return singleton;
	}

	private Singleton() {
		System.out.println("生成了一个实例");
	}

}
