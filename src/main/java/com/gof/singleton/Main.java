package com.gof.singleton;

public class Main {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Singleton singleton = Singleton.getSingleton();
		// 这样是报错的 The constructor Singleton() is not visible
		// Singleton singleton1 = new Singleton();
	}
}
